FROM ubuntu:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV DEBIAN_FRONTEND="noninteractive" \
    TERM="xterm" \
    PATH="$PATH:/usr/games" \
    DotNetVersion="7.0" \
    NodeVersion="18" \
    DOTNET_CLI_TELEMETRY_OPTOUT="true"

RUN printf "\n\033[0;36mInstalling packages...\033[0m\n" \
    && apt-get update \
    && apt-get install -y \
        wget \
        curl \
        git \
        cowsay \
        zip \
        unzip \
        openssl \
        apt-transport-https \
        dotnet-sdk-${DotNetVersion} \
    # Preparation
    && ubuntuVersion=$(cat /etc/os-release | grep VERSION_ID | cut -d "=" -f 2 | cut -d "\"" -f 2) \
    # Node.js
    && printf "\n\033[0;36mInstalling Node.js...\033[0m\n" \
    && curl -sL https://deb.nodesource.com/setup_$NodeVersion.x | bash - \
    && apt-get update \
    && apt-get install --yes nodejs \
    && npm install -g npm@latest \
    && npm --version \
    # Creating version info files
    && printf "\n\033[0;36mCreating version info files...\033[0m\n" \
    && mkdir -p /info \
    && echo "${ubuntuVersion}" > /info/ubuntu \
    && dotnet --info | grep Version | sed 's/ //g' | cut -d ":" -f 2 > /info/dotnet \
    && chmod -R 0777 /info \
    # Cleanup
    && apt-get clean all \
    && rm -rf /var/lib/apt/lists/* \
    # Done
    && printf "\n\033[0;36mDone.\033[0m\n"
